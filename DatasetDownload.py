# -*- coding: utf-8 -*-
"""
Created on Mon May  3 21:00:45 2021

@author: USER
"""
import numpy as np
import matplotlib.pyplot as plt

from tqdm.notebook import tqdm
from torchvision import datasets, transforms

mnist_train = datasets.MNIST(root="./datasets", train=True, transform=transforms.ToTensor(), download=True)
mnist_test = datasets.MNIST(root="./datasets", train=False, transform=transforms.ToTensor(), download=True)

print("Number of MNIST training examples: {}".format(len(mnist_train)))
print("Number of MNIST test examples: {}".format(len(mnist_test)))